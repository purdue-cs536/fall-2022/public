<img src="../others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 536 Course Projects @ Purdue University (Fall 2022)

1. [**Implicit Hypergraph Diffusion for Channel Decoding**](https://gitlab.com/tefnut32/channel-decoding-based-on-hypergraph-diffusion) 
1. [**Peer-to-Peer File Sharing Protocol**](https://github.com/cueo/p2p/blob/main/README.md)
1. [**Robust Networks: An Incentive and Optimization Perspective**](https://github.com/rohanvgarg/CS536-Blogpost)
1. [**HTTP over UDP: An Experimental Investigation of QUIC**](https://www.youtube.com/watch?v=8I-abG050wg)
1. [**Improving Network Efficiency through Sparsification of Network Graphs**](https://cs536-graphsparsification.github.io/improving-network-efficiency-through-sparsification-of-network-graphs/)
1. [**Switch-Aid: Accurately Identifying Heavy Hitting Flows**](https://github.com/Sripathm2/CS_536_Project/blob/main/README.md)
1. [**Understandable Bundle Protocol version 7 Implementation with Configurable Faulty Network and Extendable Evaluation**](https://www.youtube.com/watch?v=aika4nRm7wM)
1. [**Performance Evaluation of SDN Controllers using Cbench and Iperf**](https://medium.com/@dishadudhal/performance-evaluation-of-sdn-controllers-using-cbench-and-iperf-e9296f63115c)
1. [**Improving Cache Allocation for Networking Applications using Fine-grained Cache Partitioning and Unikernels**](https://youtu.be/yrQAr-tvq3I)
1. [**Reproducing "Enhancing Robustness Against Adversarial Examples in Network Intrusion Detection Systems"**](https://gist.github.com/ronaldseoh/3b45ea49730ecb0460a35ec7938ef7df)
1. [**Reproducing "Practical GAN-based Synthetic IP Header Trace Generation using NetShare"**](https://annuszulfiqar2021.github.io/NetShare/)
1. [**Reproducing "Dynamic Adaptation of Software-defined Networks for IoT Systems: A Search-based Approach"**](https://guptav96.github.io/CS536-Project/)
1. [**Reproducing "DeepQueueNet: Towards Scalable and Generalized Network Performance Estimation with Packet-level Visibility"**](https://github.com/Zhang038/reproduce_DQN/blob/main/README.md)
1. [**Reproducing "PCC Vivace: Online-Learning Congestion Control"**](https://sites.google.com/view/purdue-cs536/home)
1. [**Reproducing "pHPA: A Proactive Autoscaling Framework for Microservice Chain Context"**](https://youtu.be/8E0J8LXWF9k)
1. [**Reproducing "Evaluating QUIC Performance Over Web, Cloud Storage, and Video Workloads"**](https://youtu.be/WAvPyXUgZa8)
1. [**Reproducing "Taking a Long Look at QUIC"**](https://github.com/TownesZhou/CS536-Network-Project)
1. [**Reproducing "The TCP Outcast Problem: Exposing Unfairness in Data Center Networks"**](https://medium.com/@amanpurwar35/reproducing-the-tcp-outcast-problem-58f486d5921a)
1. [**Reproducing "An Argument for Increasing TCP's Initial Congestion Window"**](http://prishitakadam.tech/CS536-Project/)
1. [**Reproducing "RouteNet: Leveraging Graph Neural Networks for Network Modeling and Optimization in SDN"**](https://medium.com/@janani7848/reproducing-routenet-leveraging-graph-neural-networks-for-network-modeling-and-optimization-in-3451a37cc0fd)
1. [**Reproducing "Kitsune: An Ensemble of Autoencoders for Online Network Intrusion Detection"**](https://xiangzhex.notion.site/Reproducing-Kitsune-An-Ensemble-of-Autoencoders-for-Online-Network-Intrusion-Detection-d1b6a7cd02d84452868be7c99922478d)
1. [**Reproducing "TCP Fast Open"**](https://youtu.be/qVdqz3Y7JfI)
1. [**Reproducing "Implementing AES Encryption on Programmable Switches via Scrambled Lookup Tables"**](https://winston-wang-01.github.io/2022/12/09/CS536-FINAL-BLOG.html)
1. [**Reproducing "EBSNN: Extended Byte Segment Neural Network for Network Traffic Classification"**](https://youtu.be/AGJvPDSQumI)
1. [**Investigating "Taking a Long Look at QUIC"**](https://youtu.be/CJ5e01bjwcw)


<!-- 9. [**Reproducing "An Argument for Increasing TCP’s Initial Congestion Window"**](https://theaiacademy.blogspot.com/2022/12/reproducing-argument-for-increasing.html) -->
